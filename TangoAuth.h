/*
 * TangoAuth.h
 *
 *  Created on: Nov 14, 2014
 *      Author: gorbe
 */

#ifndef TANGOAUTH_H_
#define TANGOAUTH_H_

#include <tango.h>

class TangoAuthClientClass: public Tango::LogAdapter
{
public:
        TangoAuthClientClass(Tango::DeviceImpl * dev): tangodev(dev), Tango::LogAdapter(dev) {
                try {
                DEBUG_STREAM << "Trying to get authentication server name from properties" << endl;
                vector<string> auth_services;
                Tango::DbData data;
                Tango::DbDatum db_d("TangoServerAuth");
                data.push_back(db_d);
                Tango::Database *db = tangodev->get_db_device()->get_dbase();
                db->get_property("CtrlSystem",data);
                data[0] >> auth_services;
                if (auth_services.size() > 0)
                        tangoauthserver=new Tango::DeviceProxy(auth_services[0]);
                else
                {
                        DEBUG_STREAM << "Using sys/authrbac/1 as backup auth device" << endl;
                        tangoauthserver=new Tango::DeviceProxy("sys/authrbac/1");
                }

                }
                catch (...)
                {
                        DEBUG_STREAM << "Using sys/authrbac/1 as backup auth device" << endl;
                        tangoauthserver=new Tango::DeviceProxy("sys/authrbac/1");
                }
        };
	~TangoAuthClientClass() {delete tangoauthserver;};
	bool CheckAccess(string cmd_name, bool throw_exep=true) {
		string cl_ip=get_ip();
		Tango::DeviceData argin, argout;
		bool auth=false;
		vector <string> auth_rec;

		auth_rec.resize(3);
		auth_rec[0]=tangodev->get_name_lower();
		auth_rec[1]=cmd_name;
		auth_rec[2]=cl_ip;
		try
		{
			argin << auth_rec;
			argout=tangoauthserver->command_inout("check_permissions",argin);
			argout>>auth;
		}
		catch(Tango::DevFailed &e)
		{
			Tango::Except::print_exception(e);
			string err_report;
			for (unsigned char i=0;i<e.errors.length();i++)
			{
					err_report+=e.errors[i].desc;
					err_report+='\n';
			}
			Tango::Except::throw_exception("Error communicating to TangoAuthServer",err_report.c_str(),"TangoAuthClientClass::CheckAccess()");
		}
		if (throw_exep && !auth)
			Tango::Except::throw_exception("Permissions denied","Permissions denied, contact Tango administrator","TangoAuthClientClass::CheckAccess()");
		return auth;
	}


private:
	Tango::DeviceImpl * tangodev;
	Tango::DeviceProxy *tangoauthserver;

	string get_ip(void) {

		string argout="unknown";
		Tango::client_addr *cl=tangodev->get_client_ident();
		string ip=cl->client_ip;
		// possible patterns:
		// giop:unix:/tmp/omni-gorbe/000003365-1463064864
		// giop:tcp:[::ffff:159.93.50.207]:51924
		// giop:tcp:159.93.50.207:64975
		//cout << "Client IP: " << ip << endl;


		if (ip.find("giop:unix:")==0)
					return "127.0.0.1";
		if (ip.find("giop:tcp:[") == 0)
		{
			size_t lbr=0, rbr=0;
			lbr=ip.find('[',0);
			lbr=ip.find(':',lbr+1);
			lbr=ip.find(':',lbr+1);
			lbr=ip.find(':',lbr+1);
			rbr=ip.find(']',lbr);
			if (lbr!=string::npos && rbr!=string::npos && (rbr-lbr-1)>0)
				argout=ip.substr(lbr+1,rbr-lbr-1);
			return argout;
		}
		if (ip.find("giop:tcp:") == 0)
		{
			size_t lbr=0, rbr=0;
			lbr=ip.find(':',lbr+1);
			lbr=ip.find(':',lbr+1);
			rbr=ip.find(':',lbr+1);
			if (lbr!=string::npos && rbr!=string::npos && (rbr-lbr-1)>0)
				argout=ip.substr(lbr+1,rbr-lbr-1);
			return argout;
		}
		return argout;
	};
};




#endif /* TANGOAUTH_H_ */
